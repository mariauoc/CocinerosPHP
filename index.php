<!DOCTYPE html>
<!-- Ejemplo de listado paginado -->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Listado de platos</title>
    </head>
    <body>
        <h1>Listado de los platos de la base de datos</h1>
        <table>
            <tr>
                <th>Nombre</th><th>Tipo</th><th>Cocinero</th><th>Precio</th>
            </tr>
            <?php
            require_once 'bbdd.php';
            $filasPorPagina = 10;
            if (isset($_GET["contador"])) {
                $contador = $_GET["contador"];
            } else {
                $contador = 0;
            }
            $total = totalPlatos();


            $platos = selectPlatos($contador, $filasPorPagina);
            while ($fila = mysqli_fetch_assoc($platos)) {
                extract($fila);
                ?>
                <tr>
                    <td><?php echo $nombre ?></td>
                    <td><?php echo $tipo ?></td>
                    <td><?php echo $cocinero ?></td>
                    <td><?php echo $precio ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
        // Mostrando el anterior (en caso de que lo haya)
        if ($contador > 0) {
            echo "<a href='index.php?contador=" . ($contador - $filasPorPagina) . "'>Anterior </a>";
        }
        // Mostrando mensaje de los resultados actuales
        if (($contador + $filasPorPagina) <= $total) {
            echo "Mostrando de " . ($contador + 1) . " a " . ($contador + $filasPorPagina) . " de $total";
        } else {
            echo "Mostrando de " . ($contador + 1) . " a $total de $total";
        }
        // Mostrar el siguiente (en cado de que lo haya)
        if (($contador + $filasPorPagina) < $total) {
            echo "<a href='index.php?contador=" . ($contador + $filasPorPagina) . "'> Siguiente</a>";
        }
        ?>

    </body>
</html>
