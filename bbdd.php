<?php

// Función que nos devuelve los platos
// desde una posición y una cantidad determinada
function selectPlatos($inicio, $cantidad) {
    $c = conectar();
    $select = "select * from plato limit $inicio, $cantidad";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

// Función que devuelve cuántos platos hay en la bbdd
function totalPlatos() {
    $c = conectar();
    $select = "select count(*) as cantidad from plato";
    $result = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($result);
    desconectar($c);
    return $fila["cantidad"];
}


function conectar() {
    $conexion = mysqli_connect("localhost", "root", "root", "restaurant");
    if (!$conexion) {
        die("No se ha podido establecer la conexion");
    }
    return $conexion;
}

function desconectar($conexion) {
    mysqli_close($conexion);
}
